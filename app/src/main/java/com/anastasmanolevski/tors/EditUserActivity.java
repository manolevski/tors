package com.anastasmanolevski.tors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.AddEditUserTask;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Preferences;
import com.anastasmanolevski.tors.Models.User;

public class EditUserActivity extends AppCompatActivity {

    //public static final String TAG = "EditUserActivity";

    private EditText txtName;
    private EditText txtEmail;
    private SwipeRefreshLayout swipeRefreshLayoutEditUser;

    private Preferences pref;
    private User mUser;
    private AddEditUserTask mAddEditUserTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        txtName = (EditText)findViewById(R.id.txtName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        swipeRefreshLayoutEditUser = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayoutEditUser);

        pref = new Preferences(this);
        mUser = pref.ReadUser();

        populateUI();

        swipeRefreshLayoutEditUser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayoutEditUser.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_user_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveChanges();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populateUI() {
        if(mUser.getName() != null)
            txtName.setText(mUser.getName());
        if(mUser.getEmail() != null)
            txtEmail.setText(mUser.getEmail());
    }

    public void saveChanges(){
        if (mAddEditUserTask != null) {
            return;
        }

        final String name = txtName.getText().toString();
        final String email = txtEmail.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(name)) {
            txtName.setError(getString(R.string.error_field_required));
            focusView = txtName;
            cancel = true;
        }
        else if(name.length() > 60){
            txtName.setError(getString(R.string.error_long_name));
            focusView = txtName;
            cancel = true;
        }
        else if (TextUtils.isEmpty(email)) {
            txtEmail.setError(getString(R.string.error_field_required));
            focusView = txtEmail;
            cancel = true;
        }
        else if(!email.contains("@")){
            txtEmail.setError(getString(R.string.error_invalid_email));
            focusView = txtEmail;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if(!swipeRefreshLayoutEditUser.isRefreshing())
                swipeRefreshLayoutEditUser.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayoutEditUser.setRefreshing(true);
                    }
                });

            AsyncTaskListener listener = new AsyncTaskListener() {

                @Override
                public void onCompleted(String result) {
                    mAddEditUserTask = null;
                    swipeRefreshLayoutEditUser.setRefreshing(false);
                    if(result.equals("error")){
                        Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                        toast.show();
                        return;
                    }
                    mUser.setName(name);
                    mUser.setEmail(email);
                    pref.SaveUser(mUser);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCanceled() {
                    mAddEditUserTask = null;
                }
            };
            mAddEditUserTask = new AddEditUserTask(mUser.getLoginCode(), name, email, listener);
            mAddEditUserTask.execute();
        }

    }


}