package com.anastasmanolevski.tors.QRCode;

public final class Contents {
    private Contents() {
    }

    public static final class Type {
        public static final String TEXT = "TEXT_TYPE";
        private Type() {
        }
    }
}
