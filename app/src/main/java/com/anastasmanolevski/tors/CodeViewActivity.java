package com.anastasmanolevski.tors;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.DeleteCodesTask;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.StringArraySerializer;

public class CodeViewActivity extends AppCompatActivity {


    private DeleteCodesTask mDeleteCodesTask = null;
    private QRcode codeObject;
    private CodeViewFragment codeViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_view);

        Intent i = getIntent();
        codeObject = (QRcode) i.getExtras().get("code");

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        codeViewFragment = CodeViewFragment.newInstance(codeObject);
        ft.replace(R.id.code_view_fragment, codeViewFragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.code_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                editCode();
                return true;
            case R.id.action_delete:
                deleteCode();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteCode() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(R.string.code_delete_confirm);
        alert.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringArraySerializer list = new StringArraySerializer ();
                list.add(codeObject.getQrurl());

                if (mDeleteCodesTask != null) {
                    return;
                }
//                if(!mSwipeRefreshLayout.isRefreshing())
//                    mSwipeRefreshLayout.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mSwipeRefreshLayout.setRefreshing(true);
//                        }
//                    });
                AsyncTaskListener listener = new AsyncTaskListener() {

                    @Override
                    public void onCompleted(String result) {
                        mDeleteCodesTask = null;
//                        mSwipeRefreshLayout.setRefreshing(false);
                        if(result.equals("error")){
                            Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                            toast.show();
                            return;
                        }
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                    }

                    @Override
                    public void onCanceled() {
                        mDeleteCodesTask = null;
                    }
                };
                mDeleteCodesTask = new DeleteCodesTask(list, listener);
                mDeleteCodesTask.execute();
                dialog.dismiss();
            }
        });
        alert.setNegativeButton(getString(android.R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void editCode() {
        Intent intent = new Intent(this, AddEditCodeActivity.class);
        intent.putExtra("code", codeObject);
        startActivity(intent);
    }



}
