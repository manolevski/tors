package com.anastasmanolevski.tors.Utils;

public interface AsyncTaskListener {
    void onCompleted(String result);
    void onCanceled();
}
