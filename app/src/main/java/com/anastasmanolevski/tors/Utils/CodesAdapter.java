package com.anastasmanolevski.tors.Utils;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.R;

import java.util.List;
import java.util.Locale;

public class CodesAdapter extends RecyclerView.Adapter<CodesAdapter.ViewHolder> {
    protected List<QRcode> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mNameView;
        public TextView mRedirectView;
        public TextView mCountView;
        public ViewHolder(View v) {
            super(v);
            mNameView = (TextView) itemView.findViewById(R.id.code_name);
            mRedirectView = (TextView) itemView.findViewById(R.id.code_redirect);
            mCountView = (TextView) itemView.findViewById(R.id.code_count);
        }
    }

    public CodesAdapter(List<QRcode> dataSet)
    {
        items = dataSet;
    }

    @Override
    public CodesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_codes, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QRcode item = items.get(position);
        holder.mNameView.setText(item.getName());
        holder.mRedirectView.setText(item.getRedirect());
        holder.mCountView.setText(String.format(Locale.getDefault(), "%s", item.getCount().toString()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}