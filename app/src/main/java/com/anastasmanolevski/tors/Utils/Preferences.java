package com.anastasmanolevski.tors.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.anastasmanolevski.tors.Models.User;
import com.google.gson.Gson;

public class Preferences {

    public static final String SETTINGS = "settings";
    public static final String USER = "user";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    private SharedPreferences settings;

    public Preferences(Context context){
        settings = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
    }

    public void SaveUser(User user){
        SharedPreferences.Editor editor = settings.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(USER, json);
        editor.apply();
    }

    public User ReadUser(){
        String userStr = settings.getString(USER, "");
        Gson gson = new Gson();
        return gson.fromJson(userStr, User.class);
    }

    public void SaveSentToken(Boolean isSent){
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SENT_TOKEN_TO_SERVER, isSent);
        editor.apply();
    }

    public Boolean ReadSentToken(){
        return settings.getBoolean(SENT_TOKEN_TO_SERVER, false);
    }
}
