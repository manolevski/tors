package com.anastasmanolevski.tors.Utils;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.Vector;

public class StringArraySerializer extends Vector<String> implements KvmSerializable {
    String ns = "http://tors.me/";

    @Override
    public Object getProperty(int arg0) {
        return this.get(arg0);
    }

    @Override
    public int getPropertyCount() {
        return this.size();
    }

    @Override
    public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
        arg2.setName("string");
        arg2.type = PropertyInfo.STRING_CLASS;
        arg2.setNamespace(ns);
    }

    @Override
    public void setProperty(int arg0, Object arg1) {
        this.add(arg1.toString());
    }
}
