package com.anastasmanolevski.tors.Utils;

public class Configuration {
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String BASE_URL = "http://tors.me/";
    public static final String SERVICE_NAMESPACE = "http://tors.me/";
    public static final String SERVICE_URL = Configuration.BASE_URL + "TorSService.asmx";
    public static final Integer DOWNLOAD_SIZE = 1000;
}