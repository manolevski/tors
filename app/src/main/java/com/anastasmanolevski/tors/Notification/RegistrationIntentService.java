/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anastasmanolevski.tors.Notification;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.AddEditCodeTask;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.R;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Configuration;
import com.anastasmanolevski.tors.Utils.Preferences;
import com.anastasmanolevski.tors.Models.User;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    private Preferences pref;
    private QRcode codeObject;
    private User mUser;
    private AddEditCodeTask mEditCodeTask = null;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            if(intent.hasExtra("code"))
                codeObject = (QRcode) intent.getExtras().get("code");
            pref = new Preferences(getApplicationContext());
            mUser = pref.ReadUser();
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            sendRegistrationToServer(token);
            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            pref.SaveSentToken(true);
            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            pref.SaveSentToken(false);
        }
    }

    private void sendRegistrationToServer(final String token) {
        if (mEditCodeTask != null) {
            return;
        }

        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mEditCodeTask = null;

                if (result.equals("Success")) {
                    Intent registrationComplete = new Intent(Configuration.REGISTRATION_COMPLETE);
                    LocalBroadcastManager.getInstance(RegistrationIntentService.this).sendBroadcast(registrationComplete);
                }
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.error_common), Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            @Override
            public void onCanceled() {
                mEditCodeTask = null;
            }
        };
        mEditCodeTask = new AddEditCodeTask(mUser.getLoginCode(),
                codeObject.getQrurl(),
                codeObject.getName(),
                codeObject.getNotes(),
                codeObject.getRedirect(),
                token,
                codeObject.getNotify(),
                codeObject.getNotifyInterval(),
                listener);
        mEditCodeTask.execute();
    }
}
