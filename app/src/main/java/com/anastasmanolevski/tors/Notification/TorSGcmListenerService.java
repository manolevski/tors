/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anastasmanolevski.tors.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.anastasmanolevski.tors.AsyncTasks.GetCodesTask;
import com.anastasmanolevski.tors.CodeViewActivity;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.R;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Preferences;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TorSGcmListenerService extends GcmListenerService {

    private GetCodesTask mGetCodesTask = null;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");

        readCodes(message);
    }

    private void readCodes(final String message) {
        if (mGetCodesTask != null) {
            return;
        }
        Preferences pref = new Preferences(this);
        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mGetCodesTask = null;
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                        QRcode code = new QRcode();
                        if(code.convertFromJson(jsonObj)) {
                            if(code.getQrurl().equals(message))
                                sendNotification(code);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCanceled() {
                mGetCodesTask = null;
            }
        };
        mGetCodesTask = new GetCodesTask(pref.ReadUser().getLoginCode(), listener);
        mGetCodesTask.execute();


    }

    private void sendNotification(QRcode codeObject) {
        String message;
        if(codeObject.getNotifyInterval() == 1)
            message = getString(R.string.notification_message_single, codeObject.getName());
        else
            message = getString(R.string.notification_message, codeObject.getName(), codeObject.getNotifyInterval().toString());

        Intent intent = new Intent(this, CodeViewActivity.class);
        intent.putExtra("code", codeObject);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setLargeIcon(bm)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
