package com.anastasmanolevski.tors.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class QRcode implements Serializable {
    private Integer id;
    private String qrurl;
    private Integer userID;
    private String name;
    private String notes;
    private String redirect;
    private Integer count;
    private String token;
    private Boolean notify;
    private Integer notifyInterval;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQrurl() {
        return qrurl;
    }

    public void setQrurl(String qrurl) {
        this.qrurl = qrurl;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getNotify() {
        return notify;
    }

    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

    public Integer getNotifyInterval() {
        return notifyInterval;
    }

    public void setNotifyInterval(Integer notifyInterval) {
        this.notifyInterval = notifyInterval;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public boolean convertFromJson(JSONObject object) throws JSONException {
        this.setQrurl(object.getString("QRurl"));
        this.setUserID(Integer.parseInt(object.getString("UserID")));
        this.setName(object.getString("Name").trim());
        this.setNotes(object.getString("Notes").trim());
        this.setRedirect(object.getString("Redirect").trim());
        this.setToken(object.getString("Token").trim());
        this.setNotify(Boolean.parseBoolean(object.getString("Notify")));
        this.setNotifyInterval(Integer.parseInt(object.getString("NotifyInterval")));
        this.setCount(Integer.parseInt(object.getString("Count")));

        return true;
    }
}
