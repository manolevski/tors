package com.anastasmanolevski.tors.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CodeViews {
    private Date date;
    private Integer count;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public boolean convertFromJson(JSONObject object) throws JSONException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(object.getString("Date"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.setDate(convertedDate);
        this.setCount(Integer.parseInt(object.getString("Count")));

        return true;
    }
}
