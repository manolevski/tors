package com.anastasmanolevski.tors;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anastasmanolevski.tors.QRCode.Contents;
import com.anastasmanolevski.tors.QRCode.QRCodeEncoder;
import com.anastasmanolevski.tors.Utils.Configuration;
import com.anastasmanolevski.tors.Models.QRcode;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

public class ViewQRcodeActivity extends AppCompatActivity {

    private QRcode codeObject;
    private String qrUrlFull;
    private ImageView imgQrCode;
    private TextView lblSize;
    private TextView lblCodeUrl;
    private Button btnDownload;
    private ShareActionProvider mShareActionProvider;

    private Integer downloadSize;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_qrcode);

        downloadSize = Configuration.DOWNLOAD_SIZE;
        imgQrCode = (ImageView)findViewById(R.id.imgQrCode);
        lblSize = (TextView)findViewById(R.id.lblSize);
        btnDownload = (Button)findViewById(R.id.btnDownload);
        lblCodeUrl = (TextView)findViewById(R.id.lblCodeUrl);

        Intent i = getIntent();
        codeObject = (QRcode) i.getExtras().get("code");
        if(codeObject!=null)
            qrUrlFull = String.format("%s%s", Configuration.BASE_URL, codeObject.getQrurl());

        populateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        verifyStoragePermissions(this);
        return true;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    public Intent fillShareIntent(){
        String directory = Environment.getExternalStorageDirectory() + File.separator + "TorS";
        String fileName = "tors_share.png";
        Bitmap qr = generateQR(qrUrlFull, downloadSize);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("*/*");
        File tempFile = new File(directory, fileName);

        File folder = new File(directory);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            try {
                FileOutputStream out = new FileOutputStream(tempFile);
                if (qr != null)
                    qr.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
                Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                toast.show();
            }
            share.putExtra(Intent.EXTRA_TEXT, qrUrlFull);
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempFile));
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
            toast.show();
        }
        return share;
    }

    private void SaveFile(Bitmap imageToSave) {
        String directory = Environment.getExternalStorageDirectory() + File.separator + "TorS";
        String fileName = String.format("%s_%s.png", codeObject.getName(), codeObject.getQrurl());
        fileName = fileName.replaceAll(" ","_");
        File file = new File(directory, fileName);
        boolean success = true;
        if (file.exists()) {
            success = file.delete();
        }
        if(success){
            try {
                FileOutputStream out = new FileOutputStream(file);
                imageToSave.compress(Bitmap.CompressFormat.PNG, 100, out);
                Toast toast = Toast.makeText(getApplicationContext(), R.string.save_success, Toast.LENGTH_LONG);
                toast.show();
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
                Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                toast.show();
            }
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void populateUI() {
        lblSize.setText(getString(R.string.download_size, downloadSize, downloadSize));
        lblCodeUrl.setText(String.format("%s%s", Configuration.BASE_URL, codeObject.getQrurl()));
        imgQrCode.setImageBitmap(generateQR(qrUrlFull, 1080));
    }

    private Bitmap generateQR(String data, int size) {
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(data,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                size);
        try {
            return qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
            toast.show();
            return null;
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        else{
            setShareIntent(fillShareIntent());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setShareIntent(fillShareIntent());
                } else {
                    btnDownload.setEnabled(false);
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.write_permission_required, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        }
    }

    public void downloadQrCode(View view) {
        SaveFile(generateQR(qrUrlFull, downloadSize));
    }

    public void changeSize(View view) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.size);
        builder.setMessage(R.string.size_message);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.format(Locale.GERMAN, "%d", downloadSize));
        input.requestFocus();
        builder.setView(input);



        builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downloadSize = Integer.parseInt(input.getText().toString());
                lblSize.setText(String.format(Locale.GERMAN, "%dx%dpx", downloadSize, downloadSize));
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(input.getText().length() >0 && input.getText().length() < 6) {
                    Integer inputNotValidated = Integer.parseInt(input.getText().toString());
                    if (inputNotValidated < 500 || inputNotValidated > 4000) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                    } else {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                }
                else
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }
}