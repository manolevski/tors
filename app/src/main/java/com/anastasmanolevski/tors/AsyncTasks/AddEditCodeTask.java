package com.anastasmanolevski.tors.AsyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Configuration;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class AddEditCodeTask extends AsyncTask<Void, Void, String> {
    private static final String TAG = "AddEditCodeTask";
    private final String mLoginCode;
    private final String mQrCode;
    private final String mName;
    private final String mNotes;
    private final String mRedirect;
    private final String mToken;
    private final Boolean mNotify;
    private final Integer mNotifyInterval;
    private AsyncTaskListener listener;

    private static final String METHOD_NAME = "AddEditCode";

    public AddEditCodeTask(String loginCode, String qrCode, String name, String notes, String redirect, String token, Boolean notify, Integer notifyInterval, AsyncTaskListener listener) {
        mLoginCode = loginCode;
        mQrCode = qrCode;
        mName = name;
        mNotes = notes;
        mRedirect = redirect;
        mToken = token;
        mNotify = notify;
        mNotifyInterval = notifyInterval;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            SoapObject request = new SoapObject(Configuration.SERVICE_NAMESPACE, METHOD_NAME);

            PropertyInfo pi=new PropertyInfo();
            pi.setName("loginCode");
            pi.setValue(mLoginCode);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("qrCode");
            pi.setValue(mQrCode);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("name");
            pi.setValue(mName);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("notes");
            pi.setValue(mNotes);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("redirect");
            pi.setValue(mRedirect);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("token");
            pi.setValue(mToken);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("notify");
            pi.setValue(mNotify);
            pi.setType(Boolean.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("notifyInterval");
            pi.setValue(mNotifyInterval);
            pi.setType(Integer.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE androidHttpTransport = new HttpTransportSE(Configuration.SERVICE_URL);
            androidHttpTransport.call(Configuration.SERVICE_NAMESPACE + METHOD_NAME, envelope);

            Object result = envelope.getResponse();

            return result.toString();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return "error";
        }
    }

    @Override
    protected void onPostExecute(final String result) {
        listener.onCompleted(result);
    }

    @Override
    protected void onCancelled() {
        listener.onCanceled();
    }
}
