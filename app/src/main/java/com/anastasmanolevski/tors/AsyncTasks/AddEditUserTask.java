package com.anastasmanolevski.tors.AsyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Configuration;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class AddEditUserTask extends AsyncTask<Void, Void, String> {
    private static final String TAG = "AddEditUserTask";
    private final String mLoginCode;
    private final String mName;
    private final String mEmail;
    private AsyncTaskListener listener;

    private static final String METHOD_NAME = "AddEditUser";

    public AddEditUserTask(String loginCode, String name, String email, AsyncTaskListener listener) {
        mLoginCode = loginCode;
        mName = name;
        mEmail = email;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            SoapObject request = new SoapObject(Configuration.SERVICE_NAMESPACE, METHOD_NAME);

            PropertyInfo pi=new PropertyInfo();
            pi.setName("loginCode");
            pi.setValue(mLoginCode);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("name");
            pi.setValue(mName);
            pi.setType(String.class);
            request.addProperty(pi);

            pi=new PropertyInfo();
            pi.setName("email");
            pi.setValue(mEmail);
            pi.setType(String.class);
            request.addProperty(pi);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE androidHttpTransport = new HttpTransportSE(Configuration.SERVICE_URL);
            androidHttpTransport.call(Configuration.SERVICE_NAMESPACE + METHOD_NAME, envelope);

            Object result = envelope.getResponse();

            return result.toString();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return "error";
        }
    }

    @Override
    protected void onPostExecute(final String result) {
        listener.onCompleted(result);
    }

    @Override
    protected void onCancelled() {
        listener.onCanceled();
    }
}
