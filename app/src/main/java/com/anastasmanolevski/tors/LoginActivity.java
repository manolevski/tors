package com.anastasmanolevski.tors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.AddEditUserTask;
import com.anastasmanolevski.tors.Models.User;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Preferences;

public class LoginActivity extends AppCompatActivity {

    private Preferences pref;
    private SwipeRefreshLayout swipeRefreshLayoutLogin;
    private AddEditUserTask mAuthTask = null;
    private LinearLayout loginForm;
    private EditText txtLoginCode;
    private EditText txtLoginEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pref = new Preferences(this);
        swipeRefreshLayoutLogin = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayoutLogin);
        loginForm = (LinearLayout)findViewById(R.id.loginForm);
        txtLoginCode = (EditText)findViewById(R.id.txtLoginCode);
        txtLoginEmail = (EditText)findViewById(R.id.txtLoginEmail);
    }

    public void getStarted(View v) {
        if (mAuthTask != null) {
            return;
        }

        if(!swipeRefreshLayoutLogin.isRefreshing())
            swipeRefreshLayoutLogin.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayoutLogin.setRefreshing(true);
                }
            });
        AsyncTaskListener listener = new AsyncTaskListener() {
            @Override
            public void onCompleted(String result) {
                mAuthTask = null;
                swipeRefreshLayoutLogin.setRefreshing(false);
                if(result.equals("error")){
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                User user = new User();
                user.setLoginCode(result);
                pref.SaveUser(user);
                Intent intentEdit = new Intent(getApplicationContext(), EditUserActivity.class);
                startActivity(intentEdit);
                finish();
            }

            @Override
            public void onCanceled() {
                mAuthTask = null;
            }
        };
        mAuthTask = new AddEditUserTask("", "", "", listener);
        mAuthTask.execute();
    }

    public void showHideLogin(View view) {
        if(loginForm.isShown())
            loginForm.setVisibility(View.GONE);
        else
            loginForm.setVisibility(View.VISIBLE);
    }

    public void loginAgain(View view) {
        if (mAuthTask != null) {
            return;
        }
        final String loginCode = txtLoginCode.getText().toString();
        final String email = txtLoginEmail.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(loginCode)) {
            txtLoginCode.setError(getString(R.string.error_field_required));
            focusView = txtLoginCode;
            cancel = true;
        }
        else if (TextUtils.isEmpty(email)) {
            txtLoginEmail.setError(getString(R.string.error_field_required));
            focusView = txtLoginEmail;
            cancel = true;
        }
        else if(!email.contains("@")){
            txtLoginEmail.setError(getString(R.string.error_invalid_email));
            focusView = txtLoginEmail;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if (!swipeRefreshLayoutLogin.isRefreshing())
                swipeRefreshLayoutLogin.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayoutLogin.setRefreshing(true);
                    }
                });
            AsyncTaskListener listener = new AsyncTaskListener() {
                @Override
                public void onCompleted(String result) {
                    mAuthTask = null;
                    swipeRefreshLayoutLogin.setRefreshing(false);
                    if(result.equals("error")){
                        Toast toast = Toast.makeText(getApplicationContext(), R.string.error_login, Toast.LENGTH_LONG);
                        toast.show();
                        return;
                    }
                    User user = new User();
                    user.setLoginCode(loginCode);
                    user.setName(result);
                    user.setEmail(email);
                    pref.SaveUser(user);
                    Intent intentEdit = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intentEdit);
                    finish();
                }

                @Override
                public void onCanceled() {
                    mAuthTask = null;
                }
            };
            mAuthTask = new AddEditUserTask(loginCode, "", email, listener);
            mAuthTask.execute();
        }
    }
}

