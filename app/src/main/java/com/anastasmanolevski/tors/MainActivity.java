package com.anastasmanolevski.tors;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.GetCodesTask;
import com.anastasmanolevski.tors.AsyncTasks.LogoutTask;
import com.anastasmanolevski.tors.CustomRecyclerView.ClickListener;
import com.anastasmanolevski.tors.CustomRecyclerView.DividerItemDecoration;
import com.anastasmanolevski.tors.CustomRecyclerView.RecyclerTouchListener;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.Models.User;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.CodesAdapter;
import com.anastasmanolevski.tors.Utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Preferences pref;
    private ArrayList<QRcode> list = new ArrayList<>();
    private GetCodesTask mGetCodesTask = null;
    private LogoutTask mLogoutTask = null;

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CodesAdapter mAdapter;
    private TextView emptyView;
    private User mUser;

    //private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = new Preferences(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayoutMain);
        mRecyclerView = (RecyclerView) findViewById(R.id.codes_container);
        emptyView = (TextView) findViewById(R.id.empty_view);

        mAdapter = new CodesAdapter(list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        mUser = pref.ReadUser();

        setListeners();

        if(mUser == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            if(mUser.getEmail() == null || mUser.getName() == null) {
                Intent intent = new Intent(getApplicationContext(), EditUserActivity.class);
                startActivity(intent);
                finish();
            }
            else
                populateUI();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                list.clear();
                populateUI();
            }
        });
    }

    private void setListeners() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.clear();
                populateUI();
            }
        });
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(list.size() > 0) {
                    QRcode code = list.get(position);
                    Intent intent = new Intent(getBaseContext(), CodeViewActivity.class);
                    intent.putExtra("code", code);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setMessage(R.string.logout_confirm);
                alert.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogOut();
                        dialog.dismiss();
                    }
                });
                alert.setNegativeButton(getString(android.R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
                return true;
            case R.id.action_edit:
                openEditUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populateUI() {
        setTitle(mUser.getName());
        if (mGetCodesTask != null) {
            return;
        }
        if(!mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mGetCodesTask = null;
                mSwipeRefreshLayout.setRefreshing(false);
                if(result.equals("error")){
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                        QRcode code = new QRcode();
                        if(code.convertFromJson(jsonObj)) {
                            code.setId(i);
                            list.add(code);
                        }
                    }
                    if (list.isEmpty()) {
                        mRecyclerView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    }
                    else {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCanceled() {
                mGetCodesTask = null;
            }
        };
        mGetCodesTask = new GetCodesTask(mUser.getLoginCode(), listener);
        mGetCodesTask.execute();
    }

    private void LogOut() {
        if (mLogoutTask != null) {
            return;
        }
        if(!mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mLogoutTask = null;
                mSwipeRefreshLayout.setRefreshing(false);
                if(result.equals("error")){
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                pref.SaveUser(null);
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCanceled() {
                mLogoutTask = null;
            }
        };
        mLogoutTask = new LogoutTask(mUser.getLoginCode(), mUser.getEmail(), listener);
        mLogoutTask.execute();
    }

    public void openEditUser(){
        Intent intent = new Intent(getApplicationContext(), EditUserActivity.class);
        startActivity(intent);
    }

    public void addCode(View view) {
        Intent intent = new Intent(this, AddEditCodeActivity.class);
        startActivity(intent);
    }
}
