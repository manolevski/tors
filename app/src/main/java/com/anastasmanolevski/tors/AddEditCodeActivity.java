package com.anastasmanolevski.tors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.AddEditCodeTask;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.Notification.RegistrationIntentService;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.Configuration;
import com.anastasmanolevski.tors.Utils.Preferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class AddEditCodeActivity extends AppCompatActivity {

    private static final String TAG = "AddEditCodeActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private Preferences pref;
    private AddEditCodeTask mEditCodeTask = null;

    private EditText txtName;
    private EditText txtNotes;
    private EditText txtRedirect;
    private EditText txtNotifyInterval;
    private CheckBox chbNotify;
    private SwipeRefreshLayout swipeRefreshLayoutAddCode;
    private QRcode codeObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_code);

        txtName = (EditText)findViewById(R.id.txtName);
        txtNotes = (EditText)findViewById(R.id.txtNotes);
        txtRedirect = (EditText)findViewById(R.id.txtRedirect);
        txtNotifyInterval = (EditText)findViewById(R.id.txtNotifyInterval);
        chbNotify = (CheckBox)findViewById(R.id.chbNotify);
        swipeRefreshLayoutAddCode = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayoutAddCode);

        Intent i = getIntent();
        if(i.hasExtra("code"))
            codeObject = (QRcode) i.getExtras().get("code");
        if(codeObject!=null){
            populateUI();
            setTitle(R.string.edit_code);
        }
        else{
            setTitle(R.string.add_code);
        }
        pref = new Preferences(this);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (pref.ReadSentToken()) {
                    swipeRefreshLayoutAddCode.setRefreshing(false);

                    intent = new Intent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                } else {
                    swipeRefreshLayoutAddCode.setRefreshing(false);
                }
            }
        };
        registerReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_user_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveChanges();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populateUI() {
        txtName.setText(codeObject.getName());
        txtNotes.setText(codeObject.getNotes());
        txtRedirect.setText(codeObject.getRedirect());
        txtNotifyInterval.setText(codeObject.getNotifyInterval().toString());
        chbNotify.setChecked(codeObject.getNotify());
    }

    public void saveChanges(){
        String qrCode = "";
        String name = txtName.getText().toString();
        String notes = txtNotes.getText().toString();
        String redirect = txtRedirect.getText().toString();
        Boolean notify = chbNotify.isChecked();
        String notifyInterval = txtNotifyInterval.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(name)) {
            txtName.setError(getString(R.string.error_field_required));
            focusView = txtName;
            cancel = true;
        }
        else if(name.length() > 60){
            txtName.setError(getString(R.string.error_long_name));
            focusView = txtName;
            cancel = true;
        }
        else if (TextUtils.isEmpty(redirect)) {
            txtRedirect.setError(getString(R.string.error_field_required));
            focusView = txtRedirect;
            cancel = true;
        }
        else if (TextUtils.isEmpty(notifyInterval)) {
            txtNotifyInterval.setError(getString(R.string.error_field_required));
            focusView = txtNotifyInterval;
            cancel = true;
        }
        else if (Integer.parseInt(notifyInterval) < 1) {
            txtNotifyInterval.setError(getString(R.string.error_invalid_interval));
            focusView = txtNotifyInterval;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if(!swipeRefreshLayoutAddCode.isRefreshing())
                swipeRefreshLayoutAddCode.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayoutAddCode.setRefreshing(true);
                    }
                });

            if(codeObject != null) {
                codeObject.setName(name);
                codeObject.setNotes(notes);
                codeObject.setRedirect(redirect);
                codeObject.setNotifyInterval(Integer.parseInt(notifyInterval));
                codeObject.setNotify(notify);
                editCode();
            }
            else{
                if (checkPlayServices()) {
                    QRcode codeObjectAdd = new QRcode();
                    codeObjectAdd.setQrurl(qrCode);
                    codeObjectAdd.setName(name);
                    codeObjectAdd.setNotes(notes);
                    codeObjectAdd.setRedirect(redirect);
                    codeObjectAdd.setNotifyInterval(Integer.parseInt(notifyInterval));
                    codeObjectAdd.setNotify(notify);
                    // Start IntentService to register this application with GCM.
                    Intent intent = new Intent(this, RegistrationIntentService.class);
                    intent.putExtra("code", codeObjectAdd);
                    startService(intent);
                }
            }
        }
    }

    private void editCode() {
        if (mEditCodeTask != null) {
            return;
        }

        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mEditCodeTask = null;
                swipeRefreshLayoutAddCode.setRefreshing(false);
                if(result.equals("error")){
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                Intent intent = new Intent(getBaseContext(), CodeViewActivity.class);
                intent.putExtra("code", codeObject);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCanceled() {
                mEditCodeTask = null;
            }
        };
        mEditCodeTask = new AddEditCodeTask(pref.ReadUser().getLoginCode(),
                codeObject.getQrurl(),
                codeObject.getName(),
                codeObject.getNotes(),
                codeObject.getRedirect(),
                codeObject.getToken(),
                codeObject.getNotify(),
                codeObject.getNotifyInterval(),
                listener);
        mEditCodeTask.execute();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, getString(R.string.not_supported));
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.not_supported), Toast.LENGTH_SHORT);
                toast.show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Configuration.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
}
