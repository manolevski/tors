package com.anastasmanolevski.tors;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.anastasmanolevski.tors.AsyncTasks.GetViewsTask;
import com.anastasmanolevski.tors.Models.CodeViews;
import com.anastasmanolevski.tors.Models.QRcode;
import com.anastasmanolevski.tors.Utils.AsyncTaskListener;
import com.anastasmanolevski.tors.Utils.ChartValueFormatter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CodeViewFragment extends Fragment {

    private TextView lblCaption;
    private TextView lblNotes;
    private TextView lblRedirect;
    private TextView lblCode;
    private LineChart chart;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private QRcode codeObject;
    private static final String CODE_KEY = "code_key";
    private GetViewsTask mGetViewsTask = null;

    public CodeViewFragment() {
    }

    public static CodeViewFragment newInstance(QRcode code) {
        CodeViewFragment fragment = new CodeViewFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(CODE_KEY, code);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_code_view, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            codeObject = (QRcode)getArguments().getSerializable(CODE_KEY);
            mSwipeRefreshLayout = (SwipeRefreshLayout)getView().findViewById(R.id.swipeRefreshLayoutViewCode);
            lblCaption = (TextView)getView().findViewById(R.id.lblCaption);
            lblNotes = (TextView)getView().findViewById(R.id.lblNotes);
            lblRedirect = (TextView)getView().findViewById(R.id.lblRedirect);
            lblCode = (TextView)getView().findViewById(R.id.lblCode);
            chart = (LineChart)getView().findViewById(R.id.chart);

            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    populateUI();
                }
            });

            populateUI();
        }
    }

    private void populateUI() {
        lblCaption.setText(codeObject.getName());
        lblNotes.setText(codeObject.getNotes());

        String redirect = codeObject.getRedirect();
        lblRedirect.setClickable(true);
        lblRedirect.setMovementMethod(LinkMovementMethod.getInstance());
        if (!redirect.contains("http://") && !redirect.contains("https://"))
            redirect = String.format("http://%s", redirect);
        String text = String.format("<a href='%s'>%s</a>", redirect, getString(R.string.test_url));
        lblRedirect.setText(Html.fromHtml(text));

        //lblCode.setText(String.format("%s%s", Configuration.BASE_URL, codeObject.getQrurl()));
        final CharSequence textTextView = lblCode.getText();
        final SpannableString spannableString = new SpannableString( textTextView );
        spannableString.setSpan(new URLSpan(""), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        lblCode.setText(spannableString, TextView.BufferType.SPANNABLE);
        lblCode.setClickable(true);
        lblCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ViewQRcodeActivity.class);
                intent.putExtra("code", codeObject);
                startActivity(intent);
            }
        });

        if (mGetViewsTask != null) {
            return;
        }
        if(!mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        AsyncTaskListener listener = new AsyncTaskListener() {

            @Override
            public void onCompleted(String result) {
                mGetViewsTask = null;
                mSwipeRefreshLayout.setRefreshing(false);
                if(result.equals("error")){
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.error_common, Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                ArrayList<CodeViews> viewsList = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for(int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                        CodeViews views = new CodeViews();
                        if(views.convertFromJson(jsonObj)) {
                            viewsList.add(views);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FillChart(viewsList);
            }

            @Override
            public void onCanceled() {
                mGetViewsTask = null;
            }
        };
        mGetViewsTask = new GetViewsTask(codeObject.getQrurl(), listener);
        mGetViewsTask.execute();
    }

    private void FillChart(ArrayList<CodeViews> viewsList) {
        Format dateFormat = android.text.format.DateFormat.getDateFormat(getActivity().getApplicationContext());
        String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
        DateFormat df = new SimpleDateFormat(pattern);

        ArrayList<String> xValues = new ArrayList<>();
        ArrayList<Entry> valuesViews = new ArrayList<>();
        int index = 0;
        for (CodeViews views : viewsList){
            xValues.add(df.format(views.getDate()));
            Entry entry = new Entry(views.getCount(), index);
            valuesViews.add(entry);
            index++;
        }

        LineDataSet setViews = new LineDataSet(valuesViews, getString(R.string.views_chart));
        setViews.setAxisDependency(YAxis.AxisDependency.LEFT);
        setViews.setColor(Color.parseColor("#155D7F"));
        setViews.setCircleColor(Color.parseColor("#155D7F"));
        setViews.setValueTextSize(12f);
        setViews.setValueFormatter(new ChartValueFormatter());
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setViews);


        YAxis left = chart.getAxisLeft();
        left.setDrawGridLines(false); // no grid lines
        left.setTextSize(12f);
        chart.getAxisRight().setEnabled(false); // no right axis

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setTextSize(12f);

        chart.setDescription("");
        chart.setMinimumHeight(900);

        LineData data = new LineData(xValues, dataSets);
        chart.setData(data);
        chart.invalidate();
    }

}
